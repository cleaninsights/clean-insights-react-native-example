/**
 * ReactNativeStore.js
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 06.06.23.
 * Copyright © 2023 Guardian Project. All rights reserved.
 */

export {ReactNativeStore};

import {Consents, Event, Store, StoreData} from 'clean-insights-sdk';
import {Visit, VisitData} from 'clean-insights-sdk/dist/Visit';
import {EventData} from 'clean-insights-sdk/dist/Event';

var RNFS = require('react-native-fs');

class ReactNativeStore extends Store {
  static #storageFilename = 'cleaninsights-store.json';

  /**
   * @type {string}
   */
  #storageFile;

  /**
   * @param {Object.<string, any>} args
   *      The location where to read and persist accumulated data.
   *      Either in the key "storageFile", which is expected to contain the
   *      fully qualified URL (as a string) to a file.
   *      Or a "storageDir" URL (as a string), which is expected to point
   *      to a directory.
   * @param {function(string)=}debug
   *      A callback to send debug messages to.
   */
  constructor(args?: {[p: string]: any}, debug?: (message: string) => void) {
    args = args || {};

    // Support lightweight subclasses or invocations which only want
    // to override the storageFile location.
    if (typeof args.storageFile !== 'string') {
      args.storageFile = RNFS.DocumentDirectoryPath;

      if (typeof args.storageDir === 'string') {
        args.storageFile = RNFS.DocumentDirectoryPath + '/' + args.storageDir;
      }

      const len = args.storageFile.length;

      if (args.storageFile.substring(len - 1, len) !== '/') {
        args.storageFile += '/';
      }

      args.storageFile += ReactNativeStore.#storageFilename;
    }

    super(args, debug);

    // For #persist. Can't have this before the super call. Therefore all that dancing.
    this.#storageFile = args.storageFile;
  }

  /**
   * Read the stored data from disk.
   *
   * Wait for the fulfilled promise before recording anything!
   */
  read(): Promise<void> {
    return new Promise<void>(resolve => {
      RNFS.readFile(this.#storageFile, 'utf8')
        .then((content: string) => {
          const data = JSON.parse(content);

          this.consents = new Consents(data.consents);

          data.visits.forEach((visit: VisitData) => {
            this.visits.push(new Visit(visit));
          });

          data.events.forEach((event: EventData) => {
            this.events.push(new Event(event));
          });

          resolve();
        })
        .catch((error: Error) => {
          console.debug(error);

          // It's ok, if there's no file, yet.
          resolve();
        });
    });
  }

  /**
   * This will get called in the constructor, which cannot be asynchronously
   * finished.
   * Since we cannot load data from the file system synchronously in React Native,
   * we intentionally leave this a no-op.
   *
   * After initialization, call `#read()` to *actually* load the data from the file system.
   *
   * Wait for the returned promise to fulfill, to start recording data.
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  load(args: {[p: string]: any}): StoreData | undefined {
    return undefined;
  }

  /**
   * This will always be asynchronous, as react-native-fs only supports asynchronous access.
   */
  persist(async: boolean, done: (error?: Error) => void): void {
    const data = JSON.stringify(this);

    RNFS.writeFile(this.#storageFile, data, 'utf8')
      .then(() => {
        done();
      })
      .catch((error: Error) => {
        console.debug(error);

        done(error);
      });
  }

  send(
    data: string,
    server: string,
    timeout: number,
    done: (error?: Error) => void,
  ): void {
    const controller = new AbortController();

    fetch(server, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
      signal: controller.signal,
      body: data,
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 204) {
          return done(new Error(`HTTP Error ${res.status}: ${res.statusText}`));
        }

        done();
      })
      .catch(error => {
        done(error);
      });

    setTimeout(() => {
      controller.abort();
    }, timeout * 1000);
  }
}
